@echo off
echo Deploy Tool (c) ZCaliptium
echo Revision 7
echo -------------------------------------------------------------------------

REM Define Some Stuff
SET SRCDIR=.
SET BINDIR=Bin
@IF EXIST %SRCDIR%\Bin.vs2013\ SET BINDIR=Bin.vs2013
@IF EXIST %SRCDIR%\Bin.vs2015\ SET BINDIR=Bin.vs2015
@IF EXIST %SRCDIR%\Bin.vs2019\ SET BINDIR=Bin.vs2019

SET SRCBIN=%SRCDIR%\%BINDIR%
SET DEPLOY=..\DeployDir
SET DEPBIN=%DEPLOY%\%BINDIR%

REM Prepare Directories
@IF NOT EXIST %DEPLOY% mkdir %DEPLOY%
@IF NOT EXIST %DEPLOY%\Data mkdir %DEPLOY%\Data
@IF NOT EXIST %DEPLOY%\Help mkdir %DEPLOY%\Help
@IF NOT EXIST %DEPLOY%\Players mkdir %DEPLOY%\Players
@IF NOT EXIST %DEPLOY%\Temp mkdir %DEPLOY%\Temp
@IF NOT EXIST %DEPLOY%\VirtualTrees  mkdir %DEPLOY%\VirtualTrees 
@IF NOT EXIST %DEPBIN% mkdir %DEPBIN%

echo Preparing binaries...
echo -------------------------------------------------------------------------

SET COPYCOUNTER=0
SET COPIEDCOUNTER=0

REM 3rd_party
call :cntcopy %SRCBIN%\zlib.dll %DEPBIN%
call :cntcopy %SRCBIN%\libogg.dll %DEPBIN%
call :cntcopy %SRCBIN%\libvorbis.dll %DEPBIN%
call :cntcopy %SRCBIN%\libvorbisfile.dll %DEPBIN%

REM Base
call :cntcopy %SRCBIN%\Ecc.exe %DEPBIN%
call :cntcopy %SRCBIN%\Core.dll %DEPBIN%
call :cntcopy %SRCBIN%\Engine.dll %DEPBIN%
call :cntcopy %SRCBIN%\Shaders.dll %DEPBIN%

REM Gameplay
call :cntcopy %SRCBIN%\Entities.dll %DEPBIN%
call :cntcopy %SRCBIN%\Game.dll %DEPBIN%

REM Executables
call :cntcopy %SRCBIN%\Sam.exe %DEPBIN%
call :cntcopy %SRCBIN%\DedicatedServer.exe %DEPBIN%
call :cntcopy %SRCBIN%\RCon.exe %DEPBIN%

echo.
call :outcnt

echo -------------------------------------------------------------------------
echo Copying tools binaries...
echo -------------------------------------------------------------------------

SET COPYCOUNTER=0
SET COPIEDCOUNTER=0

call :cntcopy %SRCBIN%\GameGUI.dll %DEPBIN%
call :cntcopy %SRCBIN%\EngineGUI.dll %DEPBIN%

call :cntcopy %SRCBIN%\DecodeReport.exe %DEPBIN%
call :cntcopy %SRCBIN%\Depend.exe %DEPBIN%
call :cntcopy %SRCBIN%\WorldEditor.exe %DEPBIN%
call :cntcopy %SRCBIN%\Modeler.exe %DEPBIN%
call :cntcopy %SRCBIN%\SkaStudio.exe %DEPBIN%
call :cntcopy %SRCBIN%\MakeFONT.exe %DEPBIN%
echo.
call :outcnt

echo -------------------------------------------------------------------------
echo Preparing controls...
echo -------------------------------------------------------------------------

REM Prepare Directories
@IF NOT EXIST %DEPLOY%\Controls\ mkdir %DEPLOY%\Controls\
@IF NOT EXIST %DEPLOY%\Controls\System\ mkdir %DEPLOY%\Controls\System\

@IF EXIST %DEPLOY%\Controls\System\Common.ctl DEL /F %DEPLOY%\Controls\System\Common.ctl
@IF EXIST %DEPLOY%\Controls\00-Default.ctl DEL /F %DEPLOY%\Controls\00-Default.ctl
@IF EXIST %DEPLOY%\Controls\00-Default.des DEL /F %DEPLOY%\Controls\00-Default.des
@IF EXIST %DEPLOY%\Controls\01-Classics.ctl DEL /F %DEPLOY%\Controls\01-Classics.ctl
@IF EXIST %DEPLOY%\Controls\01-Classics.des DEL /F %DEPLOY%\Controls\01-Classics.des

@IF EXIST %DEPLOY%\Controls\Controls*.ctl DEL /F %DEPLOY%\Controls\Controls*.ctl

SET COPYCOUNTER=0
SET COPIEDCOUNTER=0

call :cntcopy %SRCDIR%\Controls\System\Common.ctl %DEPLOY%\Controls\System\"
call :cntcopy %SRCDIR%\Controls\00-Default.ctl %DEPLOY%\Controls\"
call :cntcopy %SRCDIR%\Controls\00-Default.des %DEPLOY%\Controls\"
call :cntcopy %SRCDIR%\Controls\01-Classics.ctl %DEPLOY%\Controls\"
call :cntcopy %SRCDIR%\Controls\01-Classics.des %DEPLOY%\Controls\"

call :cntcopy %SRCDIR%\Controls\Controls*.ctl %DEPLOY%\Controls\"

echo.
call :outcnt
echo -------------------------------------------------------------------------
echo Preparing small assets...
echo -------------------------------------------------------------------------

REM Prepare Directories
@IF NOT EXIST %DEPLOY%\Scripts\ mkdir %DEPLOY%\Scripts\
@IF NOT EXIST %DEPLOY%\Scripts\Addons\ mkdir %DEPLOY%\Scripts\Addons\
@IF NOT EXIST %DEPLOY%\Scripts\CustomOptions\ mkdir %DEPLOY%\Scripts\CustomOptions\
@IF NOT EXIST %DEPLOY%\Scripts\menu\ mkdir %DEPLOY%\Scripts\menu\
@IF NOT EXIST %DEPLOY%\Scripts\NetSettings\ mkdir %DEPLOY%\Scripts\NetSettings\

REM copy "%SRCDIR%\Classes\*.ecl" "%DEPLOY%\Classes\"
REM copy "%SRCDIR%\Data\Var\*.var" "%DEPLOY%\Data\Var\"
copy "%SRCDIR%\VirtualTrees\BasicVirtualTree.vrt" "%DEPLOY%\VirtualTrees\"
>NUL DEL /F /Q "%DEPLOY%\Scripts\CustomOptions\*.*"
@IF EXIST %SRCDIR%\Scripts\CustomOptions\*.ini copy "%SRCDIR%\Scripts\CustomOptions\*.ini" "%DEPLOY%\Scripts\CustomOptions\"
@IF EXIST %SRCDIR%\Scripts\CustomOptions\*.des copy "%SRCDIR%\Scripts\CustomOptions\*.des" "%DEPLOY%\Scripts\CustomOptions\"
@IF EXIST %SRCDIR%\Scripts\CustomOptions\*.cfg copy "%SRCDIR%\Scripts\CustomOptions\*.cfg" "%DEPLOY%\Scripts\CustomOptions\"

echo.
>NUL DEL /F /Q "%DEPLOY%\Scripts\menu\*.*"
copy "%SRCDIR%\Scripts\menu\*.*" "%DEPLOY%\Scripts\menu\"

echo.
>NUL DEL /F /Q "%DEPLOY%\Scripts\NetSettings\*.*"
copy "%SRCDIR%\Scripts\NetSettings\*.*" "%DEPLOY%\Scripts\NetSettings\"

echo -------------------------------------------------------------------------
echo Help
echo -------------------------------------------------------------------------
echo.
>NUL DEL /F /Q "%DEPLOY%\Help\*.*"
copy "%SRCDIR%\Help\*.*" "%DEPLOY%\Help\"


echo -------------------------------------------------------------------------
echo Preparing assets...
echo -------------------------------------------------------------------------

REM Prepare Directories
@IF NOT EXIST %DEPLOY%\Content\ mkdir %DEPLOY%\Content\

echo.
>NUL DEL /F /Q "%DEPLOY%\Content\*.gro"

REM Copy
copy "%SRCDIR%\Content\SEE_*.gro" "%DEPLOY%\Content\"
copy "%SRCDIR%\Content\SSE_*.gro" "%DEPLOY%\Content\"
copy "%SRCDIR%\Content\SSX_*.gro" "%DEPLOY%\Content\"

echo -------------------------------------------------------------------------

pause

:cntcopy
>NUL copy %1 %2
echo Copying %1 to %2
REM echo Exit Code is %errorlevel%
if errorlevel 0 SET /A COPIEDCOUNTER=%COPIEDCOUNTER% + 1
SET /A COPYCOUNTER=%COPYCOUNTER% + 1
exit /B

:outcnt
echo Copied %COPIEDCOUNTER%/%COPYCOUNTER%
exit /B
